
# valor1 = input('digite seu valor 1 de sua string')
# valor2 = input('digite seu valor 2 de sua string')
# valor3 = input('digite seu valor 3 de sua string')

'''
13) Faça um programa para multiplicar matrizes 3x3.
'''


def multiplacação(matriz1, matriz2):

    linhasA, colunasA = len(matriz1), len(matriz2[0])
    linhasB, colunasB = len(matriz2), len(matriz2[0])
    assert colunasA == colunasB

    matriz3 = []
    for linha in range(linhasA):
        matriz3.append([])
        for coluna in range(colunasB):
            matriz3[linha].append(0)
            for k in range(colunasA):
                matriz3[linha][coluna] += matriz1[linha][k] * \
                    matriz2[k][coluna]

    return matriz3


matriz1 = [[1, 2, 3], [2, 2, 2], [3, 6, 5]]
matriz2 = [[1, 2, 3], [2, 2, 2], [3, 6, 5]]

print(multiplacação(matriz1, matriz2))


# matriz1 = []
# for i in range(3):
#     matriz1.append([0]*3)

# matriz2 = []
# for i in range(3):
#     matriz1.append([0]*3)
