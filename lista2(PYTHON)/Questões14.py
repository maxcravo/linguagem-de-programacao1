'''
14) Faça um programa para calcular a soma, a subtração e a multiplicação de matrizes.
Você pode fornecer a matriz no código, em vez de ler o que o usuário digitar, mas suas
funções que fazem os cálculos devem ser úteis para qualquer tamanho de matriz desde que as
propriedades matemáticas sejam respeitadas.
'''


def matrizoperações(matriz1, matriz2):

    matrizSoma = []

    linhas = len(matriz1)
    coluna = len(matriz1[0])

    for i in range(linhas):
        matrizSoma.append([])
        for j in range(coluna):
            soma = matriz1[i][j] + matriz2[i][j]
            matrizSoma[i].append(soma)

    print(matrizSoma)

    matrizSubtracao = []

    linhas1 = len(matriz1)
    coluna1 = len(matriz1[0])

    for k in range(linhas1):
        matrizSubtracao.append([])
        for w in range(coluna1):
            subtracao = matriz1[k][w] - matriz2[k][w]
            matrizSubtracao[k].append(subtracao)

    print(matrizSubtracao)

    linhasA, colunasA = len(matriz1), len(matriz2[0])
    linhasB, colunasB = len(matriz2), len(matriz2[0])
    assert colunasA == colunasB

    matriz3 = []
    for linha in range(linhasA):
        matriz3.append([])
        for coluna in range(colunasB):
            matriz3[linha].append(0)
            for k in range(colunasA):
                matriz3[linha][coluna] += matriz1[linha][k] * \
                    matriz2[k][coluna]

    return matriz3


print(matrizoperações([[2, 4], [2, 6], [4, 2]], [[2, 3], [5, 5], [1, 1]]))
