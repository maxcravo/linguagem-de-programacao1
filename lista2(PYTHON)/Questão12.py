#teste = ''

'''
12) Crie um programa para ler duas strings fornecidas pelo usuário. Verifique se elas
foram um anagrama.
'''


def solucaoAnagrama1(lista1, lista2):
    lista = list(lista2)

    cont1 = 0
    percurso = True

    while cont1 < len(lista1) and percurso:
        cont2 = 0
        encontrado = False
        while cont2 < len(lista) and not encontrado:
            if lista1[cont1] == lista[cont2]:
                encontrado = True
            else:
                cont2 = cont2 + 1

        if encontrado:
            lista[cont2] = None
        else:
            percurso = False

        cont1 = cont1 + 1

    return percurso


print(solucaoAnagrama1((input('digite uma palavra')),
                       (input('digite outra palavra'))))
