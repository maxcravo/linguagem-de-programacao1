'''
15) Crie um programa que verifica todos os números perfeitos em um intervalo fornecido
pelo usuário. Ou seja, o usuário fornece dois valores (inicial e final) e o programa verifica se
existe e quais são os números perfeitos nesse intervalo. Para saber o que são números
perfeitos, busque na wikipedia.
'''


def perfeitos():

    inicial = int(input('valor incial: '))
    final = int(input('valor final: '))

    cont = 0
    i = inicial

    while i <= final:
        i += 1

        soma = 0

        for test in range(1, i):
            if i % test == 0:
                soma += test

        if i == soma:
            print('número perfeito é {}'.format(i))
            cont += 1


print(perfeitos())
